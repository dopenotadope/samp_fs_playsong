#include <a_samp>
#include <sscanf>
#include <zcmd>
// feel free to edit this FS as you want, but dont remove the credits !! <3
#define    COLOR_ERROR                0xFF0000AA
#define COLOR_YELLOW           0xFFFF00AA
/*
M A D E   B Y    Z1z, you can find me on Skype: renan.araujo279
Git: github.com/NoFaith
*/
public OnFilterScriptInit()
{
    print("\n********************************************");
    print("SIMPLE AUDIO STREAM FOR SAMP - made by Z1z - ultra-h.com");
    print("**********************************************\n");
    return 1;
}

CMD:playsong(playerid, params[])
{
    //new adminname[49];
    if(isnull(params)) return SendClientMessage(playerid, COLOR_ERROR, "[ XxX ] CMD: /playsong [URL]");
    for(new u; u < MAX_PLAYERS; u++)
    {
        PlayAudioStreamForPlayer(u, params);
    }
    SendClientMessageToAll(COLOR_YELLOW, "An admin put a song, to stop, type: /stopsong");
    return 1;
}
//It would look better if I put a box to type the CMD to play the song....but i wanted to make it simple.

CMD:stopsong(playerid, params[])
{
    StopAudioStreamForPlayer(playerid);
    SendClientMessage( playerid, COLOR_YELLOW, "[ XxX ] You stopped the current song");
    return 1;
}

CMD:ssongtoall(playerid, params[])
{
    new adminname[24], string[128];
    GetPlayerName(playerid, adminname, 24);

    if(!IsPlayerAdmin(playerid)) return SendClientMessage(playerid, COLOR_ERROR, "[ XxX ] You are not allowed");
    for( new u; u < MAX_PLAYERS; u++ )
    {
    StopAudioStreamForPlayer( u );
    }
    format(string,sizeof(string),"[ > ] The admin %s stopped the song", adminname );
    SendClientMessage(playerid, COLOR_YELLOW, string);
    return 1;
}